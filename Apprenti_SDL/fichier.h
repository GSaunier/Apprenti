#ifndef FICHIER_H
#define FICHIER_H

#include <SDL/SDL.h>
#include <SDL/SDL_image.h>
#include "constantes.h"
#include "Fonction_simple.h"

void Initialiser(int tabSquelette[NBSQUE][CARASQUE], int tabTerre[3][3], int *nb_sque, int *sque_mort, int *a, int *b, int * niveau, int *objectifsRestants, int *continuer, int *dejatoucher, int *direction_hero, int *save);
void chargement(int carte[][NB_BLOCS_HAUTEUR], int collision[][NB_BLOCS_HAUTEUR], int tabvase[][NB_BLOCS_HAUTEUR], int tabSquelette[NBSQUE][CARASQUE], int *niveau, int *vie, int *objectifsRestants, int *a, int *b, int *feu, int *glace, int *terre, int *vent, int *nb_sque, int *progression, int infotabjoueur[2], int infojoueur[2], SDL_Rect *pos, SDL_Rect *posTab);
FILE* ouvertureFichier(int niveau);
void lectureFichier(int niveau[][NB_BLOCS_HAUTEUR], int collision[][NB_BLOCS_HAUTEUR], int tabvase[][NB_BLOCS_HAUTEUR], FILE* fichier);
int chargerNiveau(int niveau[][NB_BLOCS_HAUTEUR], int collision[][NB_BLOCS_HAUTEUR], int vase[][NB_BLOCS_HAUTEUR], int numero);
void ecritureFichier(int niveau[][NB_BLOCS_HAUTEUR], FILE* fichier);
int sauvegarderNiveau(int niveau[][NB_BLOCS_HAUTEUR], int numero);
int sauvegarde_en_jeu(int niveau[][NB_BLOCS_HAUTEUR], int tabSquelette[NBSQUE][CARASQUE],SDL_Rect positionTab, SDL_Rect position,int a, int b, int objectifsRestants, int numero, int vie, int feu, int glace, int terre, int vent, int nb_sque);
int charger_progression(int niveau[][NB_BLOCS_HAUTEUR], int collision[][NB_BLOCS_HAUTEUR], int vase[][NB_BLOCS_HAUTEUR], int tabSquelette[NBSQUE][CARASQUE], int *objectifsRestants, int *numero, int *vie, int infojoueur[2], int infotabjoueurs[2], int *a, int *b, int *feu, int *glace, int *terre, int *vent, int *nb_sque);

#endif
