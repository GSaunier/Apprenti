#include"Fonction_simple.h"
int collisionJoueur(int tabSque[NBSQUE][CARASQUE], int tabTerre[3][3], SDL_Rect posSuivant, int nb_sque, int ennemi)
{
    int bloquer = 0, j = 0;

    // Vérification collision avec Squelette:
    for (j = 0 ; j < nb_sque; j ++)
    {
        if(ennemi != j)
        {
            if(posSuivant.x == tabSque[j][POSX])
            {
                if(posSuivant.y == tabSque[j][POSY])
                bloquer = 1;
            }
            else if(posSuivant.x == tabSque[j][POSX] + 2 * DEPLACEMENT &&  posSuivant.y == tabSque[j][POSY])
                bloquer = 1;
            else if(posSuivant.x == tabSque[j][POSX] +  DEPLACEMENT &&  posSuivant.y == tabSque[j][POSY])
                bloquer = 1;
            else if(posSuivant.x == tabSque[j][POSX] -  DEPLACEMENT &&  posSuivant.y == tabSque[j][POSY])
                bloquer = 1;
            else if(posSuivant.x == tabSque[j][POSX] - 2 *   DEPLACEMENT &&  posSuivant.y == tabSque[j][POSY])
                bloquer = 1;
        }
    }

     // Vérification collision avec les pierre:
    for (j = 0; j < 3; j++)
    {
        if(bloquer == 0)
        {
            if(posSuivant.x == tabTerre[j][X])
            {
                if(posSuivant.y + DEPLACEMENT == tabTerre[j][Y] || posSuivant.y + 2 * DEPLACEMENT == tabTerre[j][Y] || posSuivant.y == tabTerre[j][Y])
                    bloquer = 1;
            }
            else if(posSuivant.x - DEPLACEMENT == tabTerre[j][X])
            {
                if( posSuivant.y + DEPLACEMENT == tabTerre[j][Y] || posSuivant.y + 2 * DEPLACEMENT == tabTerre[j][Y] || posSuivant.y == tabTerre[j][Y])
                    bloquer = 1;
            }
            else if(posSuivant.x - 2 * DEPLACEMENT == tabTerre[j][X])
            {
                if( posSuivant.y + DEPLACEMENT == tabTerre[j][Y] || posSuivant.y + 2 * DEPLACEMENT == tabTerre[j][Y] || posSuivant.y == tabTerre[j][Y])
                    bloquer = 1;
            }
            else if(posSuivant.x + DEPLACEMENT == tabTerre[j][X])
            {
                if( posSuivant.y + DEPLACEMENT == tabTerre[j][Y] || posSuivant.y + 2 * DEPLACEMENT == tabTerre[j][Y] || posSuivant.y == tabTerre[j][Y])
                    bloquer = 1;
            }
            else if(posSuivant.x + 2 * DEPLACEMENT == tabTerre[j][X])
            {
                if( posSuivant.y + DEPLACEMENT == tabTerre[j][Y] || posSuivant.y + 2 * DEPLACEMENT == tabTerre[j][Y] || posSuivant.y == tabTerre[j][Y])
                    bloquer = 1;
            }
        }
    }
        return bloquer;
}

void deplacerJoueur (int carte[][NB_BLOCS_HAUTEUR], SDL_Rect *pos, SDL_Rect *posTab, int direction, int *i, int *j, int *objectif, int *direction_hero, int tabSque[NBSQUE][CARASQUE], int tabTerre[3][3], int nb_sque, int ennemi)
{
    SDL_Rect positionSuivant, posdepSuivant;
    int bloquer = 0;

    //calcul de la futur position du joueur:
    switch (direction)
    {
        case HAUT:
            positionSuivant.y = pos->y + DEPLACEMENT;
            positionSuivant.x = pos->x;
            posdepSuivant.x = pos->x;
            posdepSuivant.y = pos->y - DEPLACEMENT;
            break;
        case BAS:
            positionSuivant.y = pos->y + TAILLE_BLOC;
            positionSuivant.x = pos->x;
            posdepSuivant.x = pos->x;
            posdepSuivant.y = pos->y + DEPLACEMENT;
            break;
        case GAUCHE:
            positionSuivant.x = pos->x - DEPLACEMENT;
            positionSuivant.y = pos->y;
            posdepSuivant.x = pos->x - DEPLACEMENT;
            posdepSuivant.y = pos->y;
            break;
        case DROITE:
            positionSuivant.x = pos->x  + TAILLE_BLOC ;
            positionSuivant.y = pos->y;
            posdepSuivant.x = pos->x + DEPLACEMENT;
            posdepSuivant.y = pos->y;
            break;
    }

        switch (direction)
        {
            case HAUT:
                // On teste la présence de mur/arbre ou de bordure d'écran
                if (positionSuivant.y < 0)
                    break;
                if (carte[posTab->x][positionSuivant.y / TAILLE_BLOC] == MUR)
                    break;
                if (pos->x % TAILLE_BLOC != 0)
                {
                    if (carte[posTab->x + 1][positionSuivant.y / TAILLE_BLOC] == MUR)
                        break;
                }
                bloquer = collisionJoueur(tabSque, tabTerre, posdepSuivant, nb_sque, ennemi);
                if (bloquer == 1)
                    break;
                if (carte[posTab->x][positionSuivant.y / TAILLE_BLOC] == PORTE)
                    *objectif = 0;
                //Si tout est bon, alors on peut bouger

                *i = *i - 1;
                if (*i ==  -3 || *i == 0)
                {
                     *i = 0;
                    posTab->y--;
                }
                pos->y = pos->y - DEPLACEMENT;
                *direction_hero = 1;
                break;
            case BAS:
                if (positionSuivant.y / TAILLE_BLOC >= DEPLACEMENT * NB_BLOCS_HAUTEUR - DEPLACEMENT - 1)
                    break;
                if (carte[posTab->x][positionSuivant.y / TAILLE_BLOC] == MUR)
                    break;
                if (pos->x % TAILLE_BLOC != 0)
                {
                    if (carte[posTab->x + 1][positionSuivant.y / TAILLE_BLOC] == MUR)
                        break;
                }
                bloquer = collisionJoueur(tabSque, tabTerre, posdepSuivant, nb_sque, ennemi);
                if (bloquer == 1)
                    break;
                if (carte[posTab->x][positionSuivant.y / TAILLE_BLOC] == PORTE)
                    *objectif = 0;


                *i = *i + 1;
                if (*i ==  4 || *i == 1)
                {
                     *i = 1;
                    posTab->y++;
                }
                pos->y = pos->y +DEPLACEMENT;
                *direction_hero = 2;
                break;

            case GAUCHE:
                if (positionSuivant.x  < 0)
                    break;
                if (carte[positionSuivant.x / TAILLE_BLOC][posTab->y] == MUR)
                    break;
               bloquer = collisionJoueur(tabSque, tabTerre, posdepSuivant, nb_sque, ennemi);
                if (bloquer == 1)
                    break;
                /*if (carte[positionSuivant.x / TAILLE_BLOC][posTab->y] == PORTE)
                    carte[positionSuivant.x / TAILLE_BLOC][posTab->y] = VIDE;
                    break;*/

                *j = *j - 1;
                if (*j == -1 || *j == -4)
                {
                     *j = -1;
                    posTab->x--;
                }
                pos->x = pos->x -DEPLACEMENT;
                *direction_hero = 3;
                break;
        case DROITE:
                if (positionSuivant.x / TAILLE_BLOC >=  NB_BLOCS_LARGEUR)
                    break;
                if (carte[positionSuivant.x / TAILLE_BLOC][posTab->y] == MUR)
                    break;
               bloquer = collisionJoueur(tabSque, tabTerre, posdepSuivant, nb_sque, ennemi);
                if (bloquer == 1)
                    break;
                /*if (carte[positionSuivant.x / TAILLE_BLOC][posTab->y] == PORTE)
                    carte[positionSuivant.x / TAILLE_BLOC][posTab->y] = VIDE;
                    break;*/
                *j = *j + 1;
                if (*j == 3 || *j == 0)
                {
                     *j = 0;
                    posTab->x++;
                }
                pos->x = pos->x + DEPLACEMENT;
                *direction_hero = 4;
                break;
        }//F switch
}


int bougerEnnemis(int carte[][NB_BLOCS_HAUTEUR], int tabSque[NBSQUE][CARASQUE], int tabTerre[3][3], int i, int nb_sque, SDL_Rect *pos, SDL_Rect *posTab, SDL_Rect *posJoueur, int *dep, int *direction_sque, int *vie, int *toucher, int *dejatoucher, SDL_Rect posSuivant, SDL_Rect positionSuivant)
{
    int bloquer = 0, deplacement = 0, j = 0;

    //Vérification de la collision avec le décor:
    if (positionSuivant.x / TAILLE_BLOC >= DEPLACEMENT * NB_BLOCS_LARGEUR - DEPLACEMENT - 1);

    else if (positionSuivant.x < 0);

    else if (positionSuivant.y < 0);

    else if (posSuivant.y !=  pos->y && pos->x % TAILLE_BLOC != 0 && carte[posTab->x + 1][positionSuivant.y / TAILLE_BLOC] == MUR );

    else if (positionSuivant.y / TAILLE_BLOC >= DEPLACEMENT * NB_BLOCS_HAUTEUR - DEPLACEMENT - 1);

    else if (posSuivant.x != pos->x && carte[positionSuivant.x / TAILLE_BLOC][posTab->y] == MUR );

    else if(posSuivant.y != pos->y && carte[posTab->x][positionSuivant.y / TAILLE_BLOC] == MUR);

    else if (posSuivant.x == posJoueur->x && posSuivant.y == posJoueur->y)
    {
        *vie = *vie - 1 ;
        deplacement = 1;
        *toucher = 1;
        *dejatoucher = 0;
        if(pos->x < posSuivant.x)
            *direction_sque = 4;
        if(pos->x > posSuivant.x)
            *direction_sque = 3;
        if(pos->y < posSuivant.y)
            *direction_sque = 2;
        if(pos->y > posSuivant.y)
            *direction_sque = 1;
    }
    else
    {
        // Vérification collision avec autres squelettes:
        for (j = 0 ; j < nb_sque; j ++)
        {
           if(i != j && posSuivant.x == tabSque[j][2] && posSuivant.y == tabSque[j][3] && bloquer == 0)
                bloquer = 1;
        }

         // Vérification collision avec les blocs de pierre:
        for (j = 0; j < 3; j++)
        {
            if(bloquer == 0)
            {
                if(posSuivant.x == tabTerre[j][X])
                {
                    if(posSuivant.y == tabTerre[j][Y] || posSuivant.y + DEPLACEMENT == tabTerre[j][Y] || posSuivant.y + 2 * DEPLACEMENT == tabTerre[j][Y])
                        bloquer = 1;
                }
                else if(posSuivant.x - DEPLACEMENT == tabTerre[j][X])
                {
                    if(posSuivant.y == tabTerre[j][Y] || posSuivant.y + DEPLACEMENT == tabTerre[j][Y] || posSuivant.y + 2 * DEPLACEMENT == tabTerre[j][Y])
                        bloquer = 1;
                }
                else if(posSuivant.x - 2 * DEPLACEMENT == tabTerre[j][X])
                {
                    if(posSuivant.y == tabTerre[j][Y] || posSuivant.y + DEPLACEMENT == tabTerre[j][Y] || posSuivant.y + 2 * DEPLACEMENT == tabTerre[j][Y])
                        bloquer = 1;
                }
                else if(posSuivant.x + DEPLACEMENT == tabTerre[j][X])
                {
                    if(posSuivant.y == tabTerre[j][Y] || posSuivant.y + DEPLACEMENT == tabTerre[j][Y] || posSuivant.y + 2 * DEPLACEMENT == tabTerre[j][Y])
                        bloquer = 1;
                }
                else if(posSuivant.x + 2 * DEPLACEMENT == tabTerre[j][X])
                {
                    if(posSuivant.y == tabTerre[j][Y] || posSuivant.y + DEPLACEMENT == tabTerre[j][Y] || posSuivant.y + 2 * DEPLACEMENT == tabTerre[j][Y])
                        bloquer = 1;
                }
            }
            else;
        }

        // Si non bloquer changement des coordonnées des Squelettes:
        if(bloquer == 0)
        {
            if(pos->x < posSuivant.x) //Deplacement vers le BAS
            {
                *dep = *dep + 1;
                *direction_sque = 4;
                if (*dep == 3 || *dep == 0)
                {
                    *dep = 0;
                    posTab->x++;
                }
            }
            else if (pos->x > posSuivant.x) //Deplacement vers le HAUT
            {

                *dep = *dep - 1;
                 *direction_sque = 3;
                if (*dep == -1 || *dep == -4)
                {
                    *dep = -1;
                    posTab->x--;
                }
            }
            else if(pos->y < posSuivant.y) //Deplacement vers la droite
            {
                *dep = *dep + 1;
                 *direction_sque = 2;
                if (*dep == 4 || *dep == 1)
                {
                    *dep = 1;
                    posTab->y++;
                }
            }
            else if(pos->y > posSuivant.y) //Deplacement vers la gauche
            {
                *dep = *dep - 1;
                 *direction_sque = 1;
                if (*dep == -3 || *dep == 0)
                {
                    *dep = 0;
                    posTab->y--;
                }
            }
            deplacement = 1;

        }
    }
    return deplacement;
}


void deplacerEnnemi(int carte[][NB_BLOCS_HAUTEUR], int tabSque[NBSQUE][CARASQUE], int tabTerre[3][3], int i, int nb_sque, SDL_Rect *pos, SDL_Rect *posTab, SDL_Rect *posJoueur, int *depv, int *deph, int *fixe, int *haute, int *direction_sque, int *vie, int *toucher, int *dejatoucher)
{
    SDL_Rect positionSuivant, posSuivant;

    int deplacement = 0, distancex = 0, distancey = 0, distance = 0, bloquer = 0;

    //Calcul de la distance entre le suelette et le joueur:
    distancex = pos->x - posJoueur->x;
    if (distancex < 0)
        distancex = -distancex;
    distancey = pos->y - posJoueur->y;
    if (distancey < 0)
        distancey = -distancey;
    distance = distancex + distancey;

    //Future position du squelette:
    positionSuivant.x = pos->x;
    positionSuivant.y = pos->y;
    posSuivant.x = pos->x;
    posSuivant.y = pos->y;

    *toucher = 0;

    if (distance < 10 * TAILLE_BLOC)
    {
        while (deplacement != 1)
        {
            if (pos->x < posJoueur->x && ((*fixe == 0 || *fixe == 3 ||  *fixe == 4) && deplacement == 0)) //DROITE
            {
                positionSuivant.x = pos->x + DEPLACEMENT + TAILLE_BLOC - DEPLACEMENT;
                posSuivant.x = pos->x + DEPLACEMENT;
                //Vérification des collision:
                deplacement = bougerEnnemis(carte, tabSque, tabTerre, i, nb_sque, pos, posTab, posJoueur, deph, direction_sque, vie, toucher, dejatoucher, posSuivant, positionSuivant);
                posSuivant.x = pos->x;
                //Si bon -> Deplacement du squelette:
                if(deplacement == 1 && *toucher == 0)
                {
                    *fixe = 0;
                    pos->x = pos->x + DEPLACEMENT;
                }
            }

            if (pos->x > posJoueur->x &&((*fixe == 0 || *fixe == 3 ||  *fixe == 4) && deplacement == 0))  // GAUCHE
            {
                positionSuivant.x = pos->x - DEPLACEMENT;
                posSuivant.x = positionSuivant.x;

                deplacement = bougerEnnemis(carte, tabSque,tabTerre, i, nb_sque, pos, posTab, posJoueur, deph, direction_sque, vie, toucher, dejatoucher, posSuivant, positionSuivant);
                posSuivant.x = pos->x;
                if(deplacement == 1 && *toucher == 0)
                {
                    *fixe = 0;
                    pos->x = pos->x - DEPLACEMENT;
                }
            }

            if (pos->y < posJoueur->y && deplacement != 1 && *fixe < 3) //BAS
            {
                positionSuivant.y = pos->y + DEPLACEMENT + TAILLE_BLOC - DEPLACEMENT;
                posSuivant.y = pos->y + DEPLACEMENT;

                deplacement = bougerEnnemis(carte, tabSque, tabTerre, i, nb_sque, pos, posTab, posJoueur, depv, direction_sque, vie, toucher, dejatoucher, posSuivant, positionSuivant);
                posSuivant.y = pos->y;

                if(deplacement == 1 && *toucher == 0)
                {
                    *fixe = 0;
                    pos->y = pos->y + DEPLACEMENT;
                }
            }

            if (pos->y > posJoueur->y && deplacement != 1 && *fixe < 3) //HAUT
            {
                positionSuivant.y = pos->y - DEPLACEMENT + TAILLE_BLOC - DEPLACEMENT;
                posSuivant.y = pos->y - DEPLACEMENT;

                deplacement = bougerEnnemis(carte, tabSque, tabTerre, i, nb_sque, pos, posTab, posJoueur, depv, direction_sque, vie, toucher, dejatoucher, posSuivant, positionSuivant);
                posSuivant.y = pos->y;

                if(deplacement == 1 && *toucher == 0)
                {
                    if (*fixe == 1)
                        *haute = *haute + 1; // permet au squelette de remonter d'un cas d'un coup quand il est bloquer et qu'il cherche une solution vers le haut
                    else if (*fixe == 1 && *haute == 3)
                    {
                            *fixe = 0;
                            *haute = 0;
                    }
                   else
                        *fixe = 0;
                    pos->y = pos->y - DEPLACEMENT;
                }
            }

            // Différents cas quand les squelettes sont bloquer représentée par des fixes:

            if (pos->y == posJoueur->y && deplacement != 1 && *fixe != 4 && pos->x != posJoueur->x)
            {
                *fixe = 3;
            }
            if (*fixe == 3 && deplacement == 0) // HAUT
            {
                 positionSuivant.y = pos->y - DEPLACEMENT + TAILLE_BLOC - DEPLACEMENT;
                 posSuivant.y = pos->y - DEPLACEMENT;
                deplacement = bougerEnnemis(carte, tabSque, tabTerre, i, nb_sque, pos, posTab, posJoueur, depv, direction_sque, vie, toucher, dejatoucher, posSuivant, positionSuivant);
                posSuivant.y = pos->y;
                if(deplacement == 1 && *toucher == 0)
                {
                    pos->y = pos->y - DEPLACEMENT;
                }
                else if(deplacement == 0 || *toucher == 1)
                    *fixe = 4;
            }
            if (*fixe == 4 && deplacement != 1) // BAS
            {
                positionSuivant.y = pos->y + DEPLACEMENT + TAILLE_BLOC - DEPLACEMENT;
                posSuivant.y = pos->y + DEPLACEMENT;

                deplacement = bougerEnnemis(carte, tabSque, tabTerre, i, nb_sque, pos, posTab, posJoueur, depv, direction_sque, vie, toucher, dejatoucher, posSuivant, positionSuivant);
                posSuivant.y = pos->y;
                if(deplacement == 1 && *toucher == 0)
                    pos->y = pos->y + DEPLACEMENT;
                else
                    *fixe = 1;
            }
            if (*fixe == 1 && deplacement == 0) // DROITE
            {
                positionSuivant.x = pos->x + DEPLACEMENT + TAILLE_BLOC - DEPLACEMENT;
                posSuivant.x = pos->x + DEPLACEMENT;
               deplacement = bougerEnnemis(carte, tabSque, tabTerre, i, nb_sque, pos, posTab, posJoueur, deph, direction_sque, vie, toucher, dejatoucher, posSuivant, positionSuivant);
               posSuivant.x = pos->x;
               if(deplacement == 1 && *toucher == 0)
                {
                    pos->x = pos->x + DEPLACEMENT;
                }
                else
                    *fixe = 2;
            }

            if (*fixe == 2 && deplacement == 0) //GAUCHE
            {
                 positionSuivant.x = pos->x - DEPLACEMENT;
                posSuivant.x = positionSuivant.x;

                deplacement = bougerEnnemis(carte, tabSque, tabTerre, i, nb_sque, pos, posTab, posJoueur, deph, direction_sque, vie, toucher, dejatoucher, posSuivant, positionSuivant);
                posSuivant.x = pos->x;
                if(deplacement == 1 && *toucher == 0)
                {
                    pos->x = pos->x - DEPLACEMENT;
                }
                else
                    *fixe = 5;
            }
            if (*fixe == 5 && deplacement == 0) //DROITE
            {
                positionSuivant.x = pos->x + DEPLACEMENT + TAILLE_BLOC - DEPLACEMENT;
                posSuivant.x = pos->x + DEPLACEMENT;
               deplacement = bougerEnnemis(carte, tabSque, tabTerre, i, nb_sque, pos, posTab, posJoueur, deph, direction_sque, vie, toucher, dejatoucher, posSuivant, positionSuivant);
               posSuivant.x = pos->x;
               if(deplacement == 1 && *toucher == 0)
                {
                    pos->x = pos->x + DEPLACEMENT;
                }
                else
                    *fixe = 6;
            }
            if (*fixe == 7 && deplacement == 0) //GAUCHE
            {
                 positionSuivant.x = pos->x - DEPLACEMENT;
                posSuivant.x = positionSuivant.x;

                deplacement = bougerEnnemis(carte, tabSque, tabTerre, i, nb_sque, pos, posTab, posJoueur, deph, direction_sque, vie, toucher, dejatoucher, posSuivant, positionSuivant);
                posSuivant.x = pos->x;
                if(deplacement == 1 && *toucher == 0)
                {
                    pos->x = pos->x - DEPLACEMENT;
                }
                else
                    *fixe = 7;
            }
            if (*fixe == 6 && deplacement == 0) // DROITE
            {
                positionSuivant.x = pos->x + DEPLACEMENT + TAILLE_BLOC - DEPLACEMENT;
                posSuivant.x = pos->x + DEPLACEMENT;
               deplacement = bougerEnnemis(carte, tabSque, tabTerre, i, nb_sque, pos, posTab, posJoueur, deph, direction_sque, vie, toucher, dejatoucher, posSuivant, positionSuivant);
               posSuivant.x = pos->x;
               if(deplacement == 1 && *toucher == 0)
                {
                    pos->x = pos->x + DEPLACEMENT;
                }
                else
                    *fixe = 6;
            }
             if (*fixe == 7 && deplacement == 0) // BAS
            {
                positionSuivant.y = pos->y + DEPLACEMENT + TAILLE_BLOC - DEPLACEMENT;
                posSuivant.y = pos->y + DEPLACEMENT;

                deplacement = bougerEnnemis(carte, tabSque, tabTerre, i, nb_sque, pos, posTab, posJoueur, depv, direction_sque, vie, toucher, dejatoucher, posSuivant, positionSuivant);
                posSuivant.y = pos->y;
                if(deplacement == 1 && *toucher == 0)
                    pos->y = pos->y + DEPLACEMENT;
                else
                    *fixe = 1;
            }
            if (*fixe == 6 && deplacement == 0) // HAUT
            {
                 positionSuivant.y = pos->y - DEPLACEMENT + TAILLE_BLOC - DEPLACEMENT;
                 posSuivant.y = pos->y - DEPLACEMENT;
                deplacement = bougerEnnemis(carte, tabSque, tabTerre, i, nb_sque, pos, posTab, posJoueur, depv, direction_sque, vie, toucher, dejatoucher, posSuivant, positionSuivant);
                posSuivant.y = pos->y;
                if(deplacement == 1 && *toucher == 0)
                {
                    pos->y = pos->y - DEPLACEMENT;
                }
                else if(deplacement == 0 || *toucher == 1)
                    *fixe = 4;
            }
            if (bloquer == 1)
                deplacement = 1;
            if (pos->x == posJoueur->x && pos->y == posJoueur->y)
                deplacement = 1;
            if (*fixe == 1)
                deplacement = 1;
            if (deplacement != 1 && *fixe != 0)
                deplacement = 1;
            if (deplacement != 1 && *fixe == 0)
                *fixe = 1;
        }
    }
   else
    {
        *direction_sque = 2;
        *fixe = 0;
    }
}

void utilisationSque(SDL_Surface *ecran,int *compteur, int explo, int nb_sque, int tabSquelette[NBSQUE][CARASQUE], int collision[][NB_BLOCS_HAUTEUR], int tabTerre[3][3], SDL_Rect *positionActuelle,int *vie, int *dejatoucher, SDL_Surface *squelette, SDL_Surface *SHG, SDL_Surface *SBG, SDL_Surface *SGG, SDL_Surface *SDG, SDL_Surface *SD, SDL_Surface *SD1,SDL_Surface *SH,SDL_Surface *SH1,SDL_Surface *SB,SDL_Surface *SB1,SDL_Surface *SG,SDL_Surface *SG1)
{
    int depv = 0, deph = 0, fixe = 0, haute = 0, toucher = 0, direction_sque = 0, i = 0,j = 0;
     SDL_Rect positionTabSquelette, positionsquelette;

    if (*compteur == 1 )
            {
                for (i = 0; i < nb_sque; i++)
                {
                    if(tabSquelette[i][VIE] > 0 && tabSquelette[i][GLACER] == 0 && tabSquelette[i][VENT] == 0 && i != explo)
                    {
                        positionTabSquelette.x = tabSquelette[i][TABX];
                        positionTabSquelette.y = tabSquelette[i][TABY];
                        positionsquelette.x = tabSquelette[i][POSX];
                        positionsquelette.y = tabSquelette[i][POSY];
                        fixe = tabSquelette[i][FIXE];
                        haute = tabSquelette[i][HAUTE];
                        depv = tabSquelette[i][DEPV];
                        deph = tabSquelette[i][DEPH];
                        direction_sque = tabSquelette[i][DIRSQUE];
                        toucher = tabSquelette[i][TOUCHER];
                        deplacerEnnemi(collision, tabSquelette, tabTerre, i, nb_sque, &positionsquelette, &positionTabSquelette, positionActuelle, &depv, &deph, &fixe, &haute, &direction_sque, vie ,&toucher, dejatoucher);
                        tabSquelette[i][TABX] = positionTabSquelette.x;
                        tabSquelette[i][TABY] = positionTabSquelette.y;
                        tabSquelette[i][POSX] = positionsquelette.x;
                        tabSquelette[i][POSY] = positionsquelette.y;
                        tabSquelette[i][FIXE] = fixe;
                        tabSquelette[i][HAUTE] = haute;
                        tabSquelette[i][DEPV] = depv;
                        tabSquelette[i][DEPH] = deph;
                        if (direction_sque == tabSquelette[i][DIRSQUE])
                            tabSquelette[i][DIRSQUE] = direction_sque + 4;
                        else
                            tabSquelette[i][DIRSQUE] = direction_sque;
                        tabSquelette[i][TOUCHER] = toucher;
                    }
                    else if(tabSquelette[i][GLACER] != 0)
                        tabSquelette[i][GLACER]--;
                }
                *compteur = 0;
            }
            else
                *compteur = *compteur + 1;
            for (i = 0 ; i < nb_sque ; i++)
            {
                if(tabSquelette[i][VENT] == 0)
                {
                    positionsquelette.x = tabSquelette[i][POSX];
                    positionsquelette.y = tabSquelette[i][POSY];
                    if((tabSquelette[i][DIRSQUE] == 1 || tabSquelette[i][DIRSQUE] == 5) && tabSquelette[i][GLACER] != 0)
                        squelette = SHG;
                    else if((tabSquelette[i][DIRSQUE] == 2 || tabSquelette[i][DIRSQUE] == 6) && tabSquelette[i][GLACER] != 0)
                        squelette = SBG;
                    else if((tabSquelette[i][DIRSQUE] == 3 || tabSquelette[i][DIRSQUE] == 7) && tabSquelette[i][GLACER] != 0)
                        squelette = SGG;
                    else if((tabSquelette[i][DIRSQUE] == 4 || tabSquelette[i][DIRSQUE] == 8) && tabSquelette[i][GLACER] != 0)
                        squelette = SDG;
                    else if (tabSquelette[i][DIRSQUE] == 1)
                        squelette = SH;
                    else if(tabSquelette[i][DIRSQUE] == 2)
                        squelette = SB;
                    else if(tabSquelette[i][DIRSQUE] == 3)
                        squelette = SG;
                    else if(tabSquelette[i][DIRSQUE] == 4)
                        squelette = SD;
                    else if(tabSquelette[i][DIRSQUE] == 5)
                        squelette = SH1;
                    else if(tabSquelette[i][DIRSQUE] == 6)
                        squelette = SB1;
                    else if(tabSquelette[i][DIRSQUE] == 7)
                        squelette = SG1;
                    else if(tabSquelette[i][DIRSQUE] == 8)
                        squelette =SD1;
                    if(tabSquelette[i][VIE] > 0)
                        SDL_BlitSurface(squelette, NULL, ecran, &positionsquelette);
                    else
                    {
                         for(j = 0; j <= 12; j++)
                         {
                             tabSquelette[i][j] = 0;
                         }
                    }
                }
            }
}
