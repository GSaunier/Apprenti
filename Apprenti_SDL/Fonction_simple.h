#ifndef FONCTION_SIMPLE_H
#define FONCTION_SIMPLE_H
#include <SDL/SDL.h>
#include <SDL/SDL_image.h>
#include "constantes.h"

int collisionJoueur(int tabSque[NBSQUE][CARASQUE], int tabTerre[3][3], SDL_Rect posSuivant, int nb_sque, int ennemi);
void deplacerJoueur (int carte[][NB_BLOCS_HAUTEUR], SDL_Rect *pos, SDL_Rect *posTab, int direction, int *i, int *j, int *objectif, int *direction_hero, int tabSque[NBSQUE][CARASQUE], int tabTerre[3][3], int nb_sque, int ennemi);
void deplacerEnnemi(int carte[][NB_BLOCS_HAUTEUR], int tabSque[NBSQUE][CARASQUE],int tabTerre[3][3], int i, int nb_sque, SDL_Rect *pos, SDL_Rect *posTab, SDL_Rect *posJoueur, int *depv, int *deph, int *fixe, int *haute, int *direction_sque, int *vie, int *toucher, int *dejatoucher);
int bougerEnnemis(int carte[][NB_BLOCS_HAUTEUR], int tabSque[NBSQUE][CARASQUE],int tabTerre[3][3], int i, int nb_sque, SDL_Rect *pos, SDL_Rect *posTab, SDL_Rect *posJoueur, int *dep, int *direction_sque, int *vie, int *toucher, int *dejatoucher, SDL_Rect posSuivant, SDL_Rect positionSuivant);
void utilisationSque(SDL_Surface *ecran,int *compteur, int explo, int nb_sque, int tabSquelette[NBSQUE][CARASQUE], int collision[][NB_BLOCS_HAUTEUR], int tabTerre[3][3], SDL_Rect *positionActuelle,int *vie, int *dejatoucher, SDL_Surface *squelette, SDL_Surface *SHG, SDL_Surface *SBG, SDL_Surface *SGG, SDL_Surface *SDG, SDL_Surface *SD, SDL_Surface *SD1,SDL_Surface *SH,SDL_Surface *SH1,SDL_Surface *SB,SDL_Surface *SB1,SDL_Surface *SG,SDL_Surface *SG1);

#endif
