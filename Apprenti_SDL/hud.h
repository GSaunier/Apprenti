#ifndef HUD_H
#define HUD_H

void hud(SDL_Surface *ecran, int vie, int feu, int glace, int terre, int vent, int latence_feu, int latence_glace, int latence_terre, int latence_vent, int niveau);
void perdu(SDL_Surface *ecran, int *vie, SDL_Surface *Game_over, SDL_Rect posFond, int *niveau, int *continuer);
void findepartie(SDL_Surface *ecran, int *continuer, SDL_Surface *fin, SDL_Rect posFond);

#endif
