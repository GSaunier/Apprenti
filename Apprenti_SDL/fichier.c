#include"fichier.h"
void Initialiser(int tabSquelette[NBSQUE][CARASQUE], int tabTerre[3][3], int *nb_sque, int *sque_mort, int *a, int *b, int * niveau, int *objectifsRestants, int *continuer, int *dejatoucher, int *direction_hero, int *save)
{
    //Initialise les donn�es � chaque changement de niveau
    int i = 0;
    for (i = 0; i < *nb_sque ; i++)
    {
        tabSquelette[i][TABX] = 0;
        tabSquelette[i][TABY] = 0;
        tabSquelette[i][POSX] = 0;
        tabSquelette[i][POSY] = 0;
        tabSquelette[i][FIXE] = 0;
        tabSquelette[i][HAUTE] = 0;
        tabSquelette[i][DEPV] = 0;
        tabSquelette[i][DEPH] = 0;
        tabSquelette[i][DIRSQUE] = 0;
        tabSquelette[i][TOUCHER] = 0;
        tabSquelette[i][VIE] = 3;
        tabSquelette[i][GLACER] = 0;
        tabSquelette[i][VENT] = 0;
    }
    for(i = 0;  i<3 ;  i++)
    {
        tabTerre[i][X] = 0;
        tabTerre[i][Y] = 0;
        tabTerre[i][ETAT] = 0;
    }

    *nb_sque = 0;
    *sque_mort = 0;
    *a = 0;
    *b = 0;
    *niveau = *niveau + 1;
    *objectifsRestants = 1;
    *continuer = 1;
    *dejatoucher = 1;
    *direction_hero = 2;
    *save = 0;
}

void chargement(int carte[][NB_BLOCS_HAUTEUR], int collision[][NB_BLOCS_HAUTEUR], int tabvase[][NB_BLOCS_HAUTEUR], int tabSquelette[NBSQUE][CARASQUE], int *niveau, int *vie, int *objectifsRestants, int *a, int *b, int *feu, int *glace, int *terre, int *vent, int *nb_sque, int *progression, int infotabjoueur[2], int infojoueur[2], SDL_Rect *pos, SDL_Rect *posTab)
{
    int i = 0, j = 0;
    SDL_Rect positionTabSquelette, positionsquelette;

    if(*progression == 0) // Menu Nouvelle Partie
    {
        if (!chargerNiveau(carte, collision, tabvase, *niveau))  // V�rification et chargement du niveau
            exit(EXIT_FAILURE);

        // Recherche de la position dans la map du heros et des squelettes
        for (i = 0 ; i < NB_BLOCS_LARGEUR ; i++)
        {
            for (j = 0 ; j < NB_BLOCS_HAUTEUR ; j++)
            {
                if (carte[i][j] == HERO)
                {
                    posTab->x = i;
                    posTab->y = j;
                    if (*niveau == 1)
                        carte[i][j] = HERBE;
                    else if(*niveau == 2)
                        carte[i][j] = CHEMIN;
                    else
                        carte[i][j] = SOLDUR;
                }
                if (carte[i][j] == SQUELETTE)
                {
                    positionTabSquelette.x = i;
                    positionTabSquelette.y = j;

                    //G�n�ration sol sous le squelette:
                    if (*niveau < 3)
                        carte[i][j] = HERBE;
                    else
                        carte[i][j] = SOLDUR;
                    //Calcul position en pixel de l'image des Squelette et remplissage du tableau:
                    positionsquelette.x = positionTabSquelette.x * TAILLE_BLOC;
                    positionsquelette.y = positionTabSquelette.y * TAILLE_BLOC;
                    tabSquelette[*nb_sque][0] = positionTabSquelette.x;
                    tabSquelette[*nb_sque][1] = positionTabSquelette.y;
                    tabSquelette[*nb_sque][2] = positionsquelette.x;
                    tabSquelette[*nb_sque][3] = positionsquelette.y;
                    tabSquelette[*nb_sque][10] = 3;

                    *nb_sque = *nb_sque + 1;
                }
            }
        }
        //Calcul position en pixel de l'image du joueur
        pos->x = posTab->x * TAILLE_BLOC;
        pos->y = posTab->y * TAILLE_BLOC;
    }
    else // Menu Continuer
    {
        if (!charger_progression(carte, collision, tabvase, tabSquelette, objectifsRestants, niveau, vie, infojoueur, infotabjoueur, a, b, feu, glace, terre, vent, nb_sque))
            exit(EXIT_FAILURE);

        // R�initialiser pour les autres changement de niveau.
        *progression=0;
        //Chargement des donn�es jours:
        posTab->x = infotabjoueur[0];
        posTab->y = infotabjoueur[1];
        pos->x = infojoueur[0];
        pos->y = infojoueur[1];
    }
}

FILE* ouvertureFichier(int niveau)
{
    FILE* fichier = NULL;
    //selection du niveau a ouvrir
    switch (niveau)
    {
    case 1:
        fichier = fopen("niveaux/niveau1.lvl", "r+");
        break;
    case 2:
        fichier = fopen("niveaux/niveau2.lvl", "r+");
        break;
    case 3:
        fichier = fopen("niveaux/niveau3.lvl", "r+");
        break;
    case 4:
        fichier = fopen("niveaux/niveau4.lvl", "r+");
        break;
    case 5:
        fichier = fopen("niveaux/niveau5.lvl", "r+");
        break;
    case 6:
        fichier = fopen("niveaux/niveau6.lvl", "r+");
        break;
    case 7:
        fichier = fopen("niveaux/niveau7.lvl", "r+");
        break;
    case 8:
        fichier = fopen("niveaux/niveau8.lvl", "r+");
        break;
    case 9:
        fichier = fopen("niveaux/niveau9.lvl", "r+");
        break;
    case 10:
        fichier = fopen("niveaux/niveau10.lvl", "r+");
        break;
    }
    return fichier;
}

void lectureFichier(int niveau[][NB_BLOCS_HAUTEUR], int collision[][NB_BLOCS_HAUTEUR], int tabvase[][NB_BLOCS_HAUTEUR], FILE* fichier)
{
    char ligneFichier[NB_BLOCS_LARGEUR * NB_BLOCS_HAUTEUR +1] = {0};
    int i =0, j = 0;

    for (i = 0; i < NB_BLOCS_HAUTEUR; i++)
    {
        fgets(ligneFichier, NB_BLOCS_LARGEUR + 6, fichier);
        for (j = 0; j < NB_BLOCS_LARGEUR; j++)
        {
            switch (ligneFichier[j])
            {
            case '0':
                niveau[j][i] = 0;
                collision[j][i] = 0;
                tabvase[j][i] = 0;
                break;
            case '1':
                niveau[j][i] = 1;
                collision[j][i] = 0;
                tabvase[j][i] = 0;
                break;
            case '2':
                niveau[j][i] = 2;
                collision[j][i] = 0;
                tabvase[j][i] = 0;
                break;
            case '3':
                niveau[j][i] = 3;
                collision[j][i] = 0;
                tabvase[j][i] = 0;
                break;
            case '4':
                niveau[j][i] = 4;
                collision[j][i] = 5;
                tabvase[j][i] = 0;;
                break;
            case '5':
                niveau[j][i] = 5;
                collision[j][i] = 5;
                tabvase[j][i] = 0;;
                break;
            case '6':
                niveau[j][i] = 6;
                collision[j][i] = 6;
                tabvase[j][i] = 0;
                break;
            case '7':
                niveau[j][i] = 7;
                collision[j][i] = 0;
                tabvase[j][i] = 0;
                break;
            case '8':
                niveau[j][i] = 8;
                collision[j][i] = 0;
                tabvase[j][i] = 0;
                break;
            case '9':
                niveau[j][i] = 9;
                collision[j][i] = 5;
                tabvase[j][i] = 0;
                break;
            case 'A':
                niveau[j][i] = 10;
                collision[j][i] = 5;
                tabvase[j][i] = 0;
                break;
            case 'B':
                niveau[j][i] = 11;
                collision[j][i] = 5;
                tabvase[j][i] = 5;
                break;
            case 'C':
                niveau[j][i] = 12;
                collision[j][i] = 5;
                tabvase[j][i] = 0;
                break;
            case 'D':
                niveau[j][i] = 13;
                collision[j][i] = 5;
                tabvase[j][i] = 0;
                break;
            case 'E':
                niveau[j][i] = 14;
                collision[j][i] = 5;
                tabvase[j][i] = 0;
                break;
            case 'F':
                niveau[j][i] = 15;
                collision[j][i] = 5;
                tabvase[j][i] = 0;
                break;
            case 'G':
                niveau[j][i] = 16;
                collision[j][i] = 5;
                tabvase[j][i] = 0;
                break;
            case 'H':
                niveau[j][i] = 17;
                collision[j][i] = 5;
                tabvase[j][i] = 0;;
                break;
            case 'I':
                niveau[j][i] = 18;
                collision[j][i] = 5;
                tabvase[j][i] = 0;
                break;
            case 'J':
                niveau[j][i] = 19;
                collision[j][i] = 5;
                tabvase[j][i] = 0;
                break;
            case 'K':
                niveau[j][i] = 20;
                collision[j][i] = 0;
                tabvase[j][i] = 0;
                break;
            case 'L':
                niveau[j][i] = 21;
                collision[j][i] = 0;
                tabvase[j][i] = 0;
                break;
            }
        }
    }
}

int chargerNiveau(int niveau[][NB_BLOCS_HAUTEUR],int collision[][NB_BLOCS_HAUTEUR], int tabvase[][NB_BLOCS_HAUTEUR], int numero)
{
    FILE* fichier = NULL;

    //selection du niveau � ouvrir
    fichier = ouvertureFichier(numero);

    //V�rification du fichier
    if (fichier == NULL)
        return 0;

    //G�n�ration des diff�rents tableaux
    lectureFichier(niveau, collision, tabvase, fichier);

    fclose(fichier);
    return 1;
}

void ecritureFichier(int niveau[][NB_BLOCS_HAUTEUR], FILE* fichier)
{
    int i = 0, j =0;

    for (i = 0 ; i < NB_BLOCS_HAUTEUR ; i++)
    {
        for (j = 0 ; j < NB_BLOCS_LARGEUR ; j++)
        {
            switch(niveau[j][i])
            {
                case 0:
                    fprintf(fichier, "0");
                    break;
                case 1:
                    fprintf(fichier, "1");
                    break;
                case 2:
                    fprintf(fichier, "2");
                    break;
                case 3:
                    fprintf(fichier, "3");
                    break;
                case 4:
                    fprintf(fichier, "4");
                    break;
                case 5:
                    fprintf(fichier, "5");
                    break;
                case 6:
                    fprintf(fichier, "6");
                    break;
                case 7:
                    fprintf(fichier, "7");
                    break;
                case 8:
                    fprintf(fichier, "8");
                    break;
                case 9:
                    fprintf(fichier, "9");
                    break;
                case 10:
                    fprintf(fichier, "A");
                    break;
                case 11:
                    fprintf(fichier, "B");
                    break;
                case 12:
                    fprintf(fichier, "C");
                    break;
                case 13:
                    fprintf(fichier, "D");
                    break;
                case 14:
                    fprintf(fichier, "E");;
                    break;
                case 15:
                    fprintf(fichier, "F");
                    break;
                case 16:
                    fprintf(fichier, "G");
                    break;
                case 17:
                    fprintf(fichier, "H");
                    break;
                case 18:
                    fprintf(fichier, "I");
                    break;
                case 19:
                    fprintf(fichier, "J");
                    break;
                case 20:
                    fprintf(fichier, "K");
                    break;
                case 21:
                    fprintf(fichier, "L");
                    break;
            }
        }
        fprintf(fichier,"\n");
    }
}
//Fonction de sauvegarde de niveau dans l'�diteur:
int sauvegarderNiveau(int niveau[][NB_BLOCS_HAUTEUR], int numero)
{
    FILE* fichier = NULL;

    //selection du niveau a ouvrir
    fichier = ouvertureFichier(numero);

    if (fichier == NULL)
        return 0;

     ecritureFichier(niveau, fichier);

    fclose(fichier);
    return 1;
}

int sauvegarde_en_jeu(int niveau[][NB_BLOCS_HAUTEUR],int tabSquelette[NBSQUE][CARASQUE], SDL_Rect positionTab, SDL_Rect position, int a, int b, int objectifsRestants, int numero, int vie, int feu, int glace, int terre, int vent, int nb_sque)
{
    FILE* fichier = NULL;
    int i =0, j = 0;
    //sauvegarde dans le fichier progression.lvl dans le dossier save
    fichier = fopen("save/progression.lvl", "w");

    if (fichier == NULL)
        return 0;

    ecritureFichier(niveau, fichier);

    //Ajout des variables joueurs
    fprintf(fichier, "\n%d\n", numero);
    fprintf(fichier, "%d\n", vie);
    fprintf(fichier, "%d\n", positionTab.x);
    fprintf(fichier, "%d\n", positionTab.y);
    fprintf(fichier, "%d\n", position.x);
    fprintf(fichier, "%d\n", position.y);
    fprintf(fichier, "%d\n", a);
    fprintf(fichier, "%d\n", b);
    fprintf(fichier, "%d\n", objectifsRestants);
    fprintf(fichier, "%d\n", feu);
    fprintf(fichier, "%d\n", glace);
    fprintf(fichier, "%d\n", terre);
    fprintf(fichier, "%d\n", vent);
    fprintf(fichier, "%d\n", nb_sque);
    fprintf(fichier,"\n");
    //ajout infosquelette
    for (i = 0 ; i < 25 ; i++)
    {
        for (j = 0 ; j < CARASQUE ; j++)
        {
            fprintf(fichier, "%d ",tabSquelette[i][j]);
        }
        fprintf(fichier,"\n");
    }
    fclose(fichier);
    return 1;
}

int charger_progression(int niveau[][NB_BLOCS_HAUTEUR], int collision[][NB_BLOCS_HAUTEUR], int tabvase[][NB_BLOCS_HAUTEUR], int tabSquelette[NBSQUE][CARASQUE], int *objectifsRestants, int *numero, int *vie, int infojoueur[2], int infotabjoueurs[2], int *a, int *b, int *feu, int *glace, int *terre, int *vent, int *nb_sque)
{
    FILE* fichier = NULL;
    int i =0, j = 0, c = 0;

    //selection du niveau a ouvrir save/progression.lvl
    fichier = fopen("save/progression.lvl", "r");

    //traitement du niveau
    if (fichier == NULL)
        return 0;

    //R�cup�ration de la map sans le hero et les squelette:
    lectureFichier(niveau, collision, tabvase, fichier);

    //R�cup�ration des caract�ristiques du h�ro :
    fseek(fichier, 1, SEEK_CUR);
    int donne[CARASQUE]={0};
    fscanf(fichier, "%d", &donne[1]);
    *numero = donne[1];
    fscanf(fichier,"%d",&donne[1]);
    *vie = donne[1];
    fscanf(fichier,"%d",&donne[1]);
    infotabjoueurs[0] = donne[1];
    fscanf(fichier,"%d",&donne[1]);
    infotabjoueurs[1] = donne[1];
    fscanf(fichier,"%d",&donne[1]);
    infojoueur[0] = donne[1];
    fscanf(fichier,"%d",&donne[1]);
    infojoueur[1] = donne[1];
    fscanf(fichier,"%d",&donne[1]);
    *a = donne[1];
    fscanf(fichier,"%d",&donne[1]);
    *b = donne[1];
    fscanf(fichier,"%d",&donne[1]);
    *objectifsRestants = donne[1];
    fscanf(fichier,"%d",&donne[1]);
    *feu = donne[1];
    fscanf(fichier,"%d",&donne[1]);
    *glace = donne[1];
    fscanf(fichier,"%d",&donne[1]);
    *terre = donne[1];
    fscanf(fichier,"%d",&donne[1]);
    *vent = donne[1];
    fscanf(fichier,"%d",&donne[1]);
    *nb_sque = donne[1];

    //R�cup�ration des caract�ristiques des squelettes:
    fseek(fichier, 1, SEEK_CUR);
    for (i = 0; i < 25; i++)
    {
        fscanf(fichier,"%d %d %d %d %d %d %d %d %d %d %d %d %d ",&donne[0],&donne[1],&donne[2],&donne[3],&donne[4],&donne[5],&donne[6],&donne[7],&donne[8],&donne[9],&donne[10],&donne[11],&donne[12]);
        for(j = 0 ; j < CARASQUE; j++)
            tabSquelette[i][j] = donne[j];
    }
    fclose(fichier);
    if(infojoueur[0] % TAILLE_BLOC == 0)
        *a = 0;
    if(infojoueur[0] % TAILLE_BLOC == 11)
        *a = -2;
    if(infojoueur[0] % TAILLE_BLOC == 22)
        *a = -1;
    if(infojoueur[1] % TAILLE_BLOC == 0)
        *b = 0;
    if(infojoueur[1] % TAILLE_BLOC == 11)
        *b = -2;
    if(infojoueur[1] % TAILLE_BLOC == 22)
        *b = -1;
    c = *a;
    *a = *b;
    *b = c;

    printf("a: %d    b: %d\n", *a, *b);
    return 1;
}
